#!/usr/bin/env ruby
require 'celluloid'
require 'date'
require 'rubygems'
require 'logger'
require 'getoptlong'
require_relative 'DatabaseUtils'
require 'digest'

def generate_day(hostname, year, month, day, a_time)
  list_files = Array.new()
  (0.. (Integer(a_time)-1)).each { |time|
    date_file = DateTime.new(Integer(year), Integer(month), Integer(day), Integer(time))
    date_c = date_file.strftime("%d-%m-%Y")
    file_name = hostname + ".availability." + date_file.strftime("%Y%m%d-%H")
    list_files.push([file_name, date_c])
  }
  return list_files
end

class LogbookProcessing
  include Celluloid

  def initialize(filename)
    @filename = filename
  end

  def load_file
    if File.exist?(@filename)
      @file_contents = File.readlines @filename
    else
      @file_contents = NIL
    end
  end

  def print_downtime
    if @file_contents != NIL
      downtime_count = @file_contents.length
      @file_contents.each { |file_line|
        if file_line.split("|")[3].strip() == 'DOWN'
        else
          downtime_count -=1
        end
      }
      if @file_contents.length < 60
        downtime_count += (60 - @file_contents.length)
      end
      return downtime_count
    else
      downtime_count = 60
      return downtime_count
    end
  end

end

def search_d(table, date_down, min_down, time_down, file_log)
  db_object = DatabaseUtils.new
  statements = " * "
  provisory = " WHERE " + "date_downtime = " + "\'" + date_down + "\'" + " and " + "time_file = " + "\'" + time_down + "\'" + " and " + "time_downtime = " + "\'"+ min_down + "\'"  + "  and  " + "file_name = " + "\'"+ file_log + "\'"
  data = ""
  result = db_object.search(table, statements, provisory, data)
  if result.length != 0
    return TRUE
  else
    return FALSE
  end
end

def search_status(table, date_down, min_down, serverid)
  db_object = DatabaseUtils.new
  statements = " * "
  provisory = " WHERE " + "date = " + "\'" + date_down + "\'" + " and " + "time_down= " + "\'" + min_down + "\'" + " and " + "servers_id = " + "\'"+ serverid+ "\'"
  data = ""
  result = db_object.search(table, statements, provisory, data)
  if result.length != 0
    return TRUE
  else
    return FALSE
  end
end

def insert_downtime(min_down, file_logbook, date_down)
  db_object = DatabaseUtils.new
  table = "dowtime_logbook"
  statments = '' ' (hostname, file_name, time_downtime, date_downtime, time_file)  ' ''
  provisory = '' ' ($1,$2,$3,$4,$5) ' ''
  aux = file_logbook.split(".")
  hostname = aux[0]
  aux_date_time = aux[2].split("-")
  time_d_aux = aux_date_time[1]
  t_down= DateTime.new(1, 1, 1, time_d_aux.to_i)
  time_down = t_down.strftime("%H:%M")
  data = hostname, file_logbook, min_down, date_down.to_s, time_down.to_s
  result_s = search_d(table, date_down, min_down, time_down, file_logbook)
  if result_s.to_s == 'false'
    db_object.insert(table, statments, provisory, data)
  end

end

def insert_service_status(min_down, serverid, files_down, date_down, hostname)
  db_object = DatabaseUtils.new
  table = "services_status"
  statments = '' ' (date, time_down, files_down, servers_id, hostname)  ' ''
  provisory = '' ' ($1,$2,$3,$4,$5) ' ''
  data = date_down, min_down, files_down, serverid, hostname
  result_s = search_status(table,date_down,min_down,serverid)
  if result_s.to_s == 'false'
    db_object.insert(table, statments, provisory, data)
  end

end


def run(days, month, year, dir_logbook, hostname, serverid)
  final_day = days
  date_down = ""
  (1..Integer(final_day)).each do |day|
    down_td = Array.new
    list_files_downtime = Array.new
    files_list = generate_day(hostname, year, month, day.to_s, "24")
    files_list.each { |files|
      date_down = files[1]
      if File.exist?(dir_logbook + files[0])
        file_name = LogbookProcessing.new dir_logbook + files[0]
        file_name.load_file
        res = Integer(file_name.print_downtime)
        down_td.push(res)
#      puts Benchmark.measure { insert_downtime(res.to_s, files[0], files[1]) }
        if res > 0
          insert_downtime(res.to_s, files[0], files[1])
          down_td.push(res)
          md5 =  Digest::MD5.file(dir_logbook + files[0]).hexdigest
          if res != 0
            list_files_downtime.push('\'' + files[0].to_s + '\'' + ":"  + '\'' + md5 + '\'' )
          end
        end
      else
        down_td.push(60)
        insert_downtime("60", files[0], files[1])
        list_files_downtime.push('\'' + files[0].to_s + '\'' + ":"  + '\'' + '\'' )
      end

    }
    if list_files_downtime.size > 0
      files_down = list_files_downtime.to_s.sub!("[", "{").sub!("]", "}").sub!("\"", "").sub!("\"", "")
    else
      files_down = list_files_downtime.to_s.delete!("[").delete("]")
    end

    insert_service_status(down_td.inject(:+).to_s, serverid, files_down.to_s.delete("\""), date_down, hostname)
  end
end


opts = GetoptLong.new(
    ['--help', '-h', GetoptLong::NO_ARGUMENT],
    ['--year', '-Y', GetoptLong::REQUIRED_ARGUMENT],
    ['--month', '-M', GetoptLong::REQUIRED_ARGUMENT],
    ['--days', '-D', GetoptLong::REQUIRED_ARGUMENT],
    ['--dir', '-d', GetoptLong::REQUIRED_ARGUMENT],
    ['--hostname', '-H', GetoptLong::REQUIRED_ARGUMENT],
    ['--serverid', '-I', GetoptLong::REQUIRED_ARGUMENT]
)

opts1 = Hash.new
opts.each do |opt, arg|

  if opt == "--year"
    opts1["year"] = arg.to_i
  end
  if opt == "--month"
    opts1["month"] = arg.to_i
  end
  if opt == "--days"
    opts1["days"] = arg.to_i
  end
  if opt == "--dir"
    opts1["dir_name"] = arg.to_s
  end
  if opt == "--hostname"
    opts1["hostname"] = arg.to_s
  end

  if opt == "--serverid"
    opts1["serverid"] = arg.to_s
  end
  if opt == "--help"
    puts <<-EOF
hello [OPTION] ... DIR

-h, --help:
   Show help

-Y, --year x:
   Year to process SLA files

-M, --month x:
   Month to process SLA files

-D, --days x:
 Total days of moth.

-d, --dir [dir_name]:
  Directory name to SLA files.

-H, --hostname [name]
  Server name to process SLA files.

-I, --serverid [id]
  Inventary Id from server.
    EOF
  end

end

if opts1['year'] != NIL and opts1['month'] != NIL and opts1['days'] != NIL and opts1['dir_name'] != NIL and opts1['hostname'] != NIL and opts1['serverid'] != NIL
#  puts Benchmark.measure { run(opts1['days'], opts1['month'], opts1['year'], opts1['dir_name'], opts1['hostname'], opts1['serverid']) }
  run(opts1['days'], opts1['month'], opts1['year'], opts1['dir_name'], opts1['hostname'], opts1['serverid'])
else
  puts "Missing argument or arguments (try --help)"
end

