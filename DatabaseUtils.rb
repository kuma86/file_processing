require 'pg/em'
require 'configparser'

class DatabaseUtils
  def initialize()
    begin

      file_cfg = ConfigParser.new('/etc/dashboard/dashboard.conf')
      user = file_cfg['credentials']['db_user']
      passwd = file_cfg['credentials']['db_password']
      server = file_cfg['credentials']['server_db']
      port = file_cfg['credentials']['port']
      db = file_cfg['credentials']['db']

      @conn = PG::EM::Client.new(
          :dbname => db,
          :user => user,
          :password => passwd,
          :port => port,
          :host => server,
      )
    rescue PG::Error => e
    end
  end

  def insert(table, statments, provisory, data)
    begin
      EM.run do
        Fiber.new do
          statment = "INSERT INTO  " + table + statments + "VALUES" + " " + provisory
          @conn.prepare('statment', statment)
          @conn.exec_prepared_defer('statment', data)
          @conn.close
          EM.stop
        end.resume
      end
    rescue PG::Error => e
      puts e
    end
  end

  def search(table, statements, provisory, data)
    r = ""
    if provisory != "" and data !=""
      begin
        EM.run do
          Fiber.new do
            statement = "SELECT " + statements + "  FROM  " + table + provisory
            result = @conn.exec(statement, data)
            result.each { |row|
              r = row.to_s.delete("\}").delete("{").delete("\"")
            }
            @conn.close
            EM.stop
          end.resume
        end
      rescue PG::Error => e
        puts e
      end
    end
    if provisory != "" and data == ""
      begin
        EM.run do
          Fiber.new do
            statement = "SELECT  " + statements + "  FROM  " + table + provisory
            result = @conn.exec(statement)
            result.each { |row|
              r = row.to_s.delete("\}").delete("{").delete("\"")
            }
            @conn.close
            EM.stop
          end.resume
        end
      rescue PG::Error => e
        puts e
      end
    end
    if provisory == ""
      begin
        EM.run do
          Fiber.new do
            statement = "SELECT" + statements + "  FROM" + table
            h = @conn.exec(statement)
            h.each { |row|
              r = row.to_s.delete("\}").delete("{").delete("\"")
            }
            @conn.close
            EM.stop
          end.resume
        end
      rescue PG::Error => e
        puts e
      end
    end
    return r
  end

  def update(table, statements, provisory, data)
    EM.run do
      Fiber.new do
        statement = "UPDATE  " + table + " SET " + statements + provisory
        @conn.exec(statement, data)
        @conn.close
        EM.stop
      end.resume
    end
  end
end
